package happyfamily;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

abstract class Pet {

    static{

        System.out.println("Class "+ Pet.class.getSimpleName()+" is loading.");

    }
    {

        System.out.println("New " + this.getClass().getSimpleName() +" class creating.");

    }

    private Species species= Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Species getSpecies() {
        return species;
    }


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    void eat(){
        System.out.println("Я кушаю!");
    }

    abstract void respond();

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this);
        super.finalize();
    }

    @Override
    public String toString(){
        return (species+"{nickname='"+nickname+"', age="+age+", trickLevel="+trickLevel+", habits="+ habits+"}");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickname, age);
    }

    Pet(String nickname, int age){
        this.nickname=nickname;
        this.age=age;
    }

    public Pet(String nickname, int age, int trickLevel, HashSet<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    Pet(){}
}

