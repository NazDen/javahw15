package happyfamily;

import java.util.HashSet;

public class Fish extends Pet {

    @Override
    void respond() {
        System.out.println("Буль-буль.");
    }

    public Fish(String nickname, int age, int trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.FISH);
    }
}
