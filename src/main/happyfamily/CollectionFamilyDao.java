package happyfamily;


import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families= new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
      return index < 0 || index >= families.size()?  null: families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index < 0 || index >= families.size()){
            return false;
        }
       families.remove(index);
       return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return families.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
    if(families.contains(family)){
        int index= families.indexOf(family);
        families.set(index,family);
    }
    else families.add(family);
    }


    @Override
    public String toString() {
        return "CollectionFamilyDao{" +
                "families=" + families +
                '}';
    }

    public CollectionFamilyDao() {
    }

    public CollectionFamilyDao(List<Family> families) {
        this.families = families;
    }
}
